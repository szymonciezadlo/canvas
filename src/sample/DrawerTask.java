package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.GraphicsContext;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class DrawerTask extends Task {
    GraphicsContext gc;
    Random random = new Random();
    double k=0;
    double pf,x,y,points=10E6;
    double a=-8,b=8;

    public DrawerTask(GraphicsContext gc) {
        this.gc = gc;

    }

    @Override
    protected Object call(){
        BufferedImage bi = new BufferedImage(600, 1000,
                BufferedImage.TYPE_INT_RGB);
        for(int i=0;i<points;i++) {
            x = Rand();
            y = Rand();
            if (Equation.calc(x, y)) {
                k++;
                // 5 Skalowanie wartości x z zakresu <A,B> do zakresu <C,D>
                //x' = ((D-C) * (x-A) / (B-A) + C)
                x = (gc.getCanvas().getWidth() * (x) / (b-a));
                y = (gc.getCanvas().getHeight() * (y) / (b-a));
                bi.setRGB((int)(gc.getCanvas().getWidth()/2-x), (int)(gc.getCanvas().getHeight()/2-y), Color.YELLOW.getRGB());
                if (i % 5000 == 0){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            gc.drawImage(SwingFXUtils.toFXImage(bi, null), 0, 0);
                        }
                    });
                }


            }
            if (isCancelled()) break;
            updateProgress(i, points);

        }
        return 16*16*k/points;

    }
    double Rand(){

        double a=-8 + (8 + 8) * random.nextDouble();
        return a;

    }

}
