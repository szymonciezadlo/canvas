package sample;

import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;


import java.awt.*;
import java.awt.image.BufferedImage;


public class Controller {
    private DrawerTask task;
    @FXML
    private Canvas canvas;
    @FXML
    private ProgressBar progressBar=new ProgressBar(0);
    @FXML
    private TextField wynik;

    @FXML
    private void handleRunBtnAction(){
        GraphicsContext gc =
                canvas.getGraphicsContext2D();
        task = new DrawerTask(gc);
        progressBar.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                wynik.appendText(task.getValue()+"");
            }
        });

        new Thread(task).start();


    }

    @FXML
    private void handleStopBtnAction(){
        task.cancel();
    }


}
